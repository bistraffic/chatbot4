module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": "eslint-config-google",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
      "linebreak-style": "off"
    }
};
